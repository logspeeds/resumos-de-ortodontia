import React from 'react'
import { Routes, Route } from "react-router-dom"

import MBT from './components/mbt'
import Home from './components/Home'
import Informations from './components/Informations'

export default function Routers() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="mbt" element={<MBT />} />
      <Route path="informations" element={<Informations />} />
      <Route
        path="*"
        element={
          <main style={{ padding: "1rem" }}>
            <p>Página não encontrada!</p>
          </main>
        }
      />
    </Routes>
  )

}

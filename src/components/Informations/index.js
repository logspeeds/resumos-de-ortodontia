import * as React from 'react';
import { Link } from 'react-router-dom';

export default function Informations() {
  return (
    <main style={{ padding: "1rem 0" }}>
      <h2>Informations</h2>
      <Link to="/">HOME</Link>
    </main>
  );
}
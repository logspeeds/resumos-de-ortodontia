import React from 'react'

export default function SistemaMBT() {
  return (
    <div>
      <h2>Sistema MBT</h2>
      <div>Recomenda-se que as asas dos brackets gêmeos sejam colocadas paralelamente
        ao eixo maior da coroa clínica e o centro do bracket sobre o centro da coroa clínica.
      </div>
      <div>

        <h4>Erros de posicionamento</h4>

        <div><b>Erros horizontais:</b>quando as asas do brackets não estão equidistantes em relação a mesial e distal (giro do dente). </div>
        <div><b>Erros axiais ou de paralelismo:</b> quando as asas do brackets não estão paralelas ao eixo da coroa (inclinação do dente).</div>
        <div><b>Erros de espessura:</b> excesso de adesivo sob a base do bracket – a curvatura da superfície do dente não se adapta à base do bracket.</div>
        <div><b>Erros verticais (considerações gengivais):</b> dentes parcialmente erupcionados, inflamação gengival, dentes com raízes deslocadas para lingual ou palatina, coroas clinicas proporcionalmente longas e coroas clinicas proporcionalmente pequenas </div>

        <h4>Quando há fraturas incisais ou desgaste dos dentes:</h4>
        <div>- Restauração da coroa ao seu tamanho original;</div>
        <div>- Estimativa do comprimento da coroa;</div>
        <div>- Pré-ajuste a forma dos incisivos;</div>

        <h4>Vantagens em usar a tabela recomendada de posicionamento vertical dos brackets</h4>

        <div>- Erros gengivais potenciais são eliminados porque as medidas são realizadas a partir das superfícies oclusais</div>
        <div>- Erros decorrentes da existência de dentes proporcionalmente grandes ou pequenos em relação ao resto da dentição são eliminados</div>
        <div>- É necessário colocar os brackets fora do centro da coroa clinica de alguns dentes</div>
        <div>- Usa-se a técnica de posicionamento vertical de brackets fora do centro da coroa clinica de alguns dentes para obter uma oclusão adequada e evitar interferências oclusais, falta de contato oclusal e diferenças de altura antiestéticas.</div>

        <h4>Como escolher a linha da tabela:</h4>

        <div>- A altura da coroa clinica é medida no modelo de estudo do paciente com um compasso e régua milimetrada;</div>
        <div>- O valor é divido ao meio e arredondado para o meio milímetro mais próximo. Isso fornece a medida da borda incisal até o centro da coroa clinica;</div>
        <div>- As medidas obtidas são usadas para decidir qual linha da tabela é apropriada para o paciente;</div>
        <div>- A linha que contém o numero máximo de valores registrados é escolhida.</div>
      </div>
    </div>

  )
}

import * as React from 'react';

import { Link } from "react-router-dom";

import { styled } from '@mui/material/styles';
import { makeStyles } from '@material-ui/styles';

import Box from '@mui/material/Box';
import MuiAppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';

import MenuIcon from '@mui/icons-material/Menu';

import './style.css'

import pucLogo from '../../img/pucLogo.jpg'

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

export default function HeaderAppBar(props) {
  const useStyles = makeStyles({
    root: {
      background: '#BA0C1E',
      minWidth: '300px',
      height: '40px',
      top: '20px',
    },
    toolbar: {
      height: '40px',
      display: 'flex',
      flexDirection: 'Row',
      alignItems: 'center',
    },
  });

  const pages = ['Home', 'Contato'];

  const classes = useStyles();

  return (
    <AppBar className={classes.root} open={props.drawerState}>
      <Container className={classes.root}>
        <Container className={classes.toolbar}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={props.drawerOpen}
            edge="start"
            sx={{ mr: 2, ...(props.drawerState && { display: 'none' }) }}
          >
            <MenuIcon />
          </IconButton>
          <img src={pucLogo} alt="Pontifícia Universidade Católica do Paraná" width="30" />
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{
              display:
              {
                xs: 'none',
                md: 'flex',
                fontFamily: 'Bebas Neue',
                paddingLeft: '30px',
                width: '100%'
              }
            }}
          >
            RESUMOS DE ORTODONTIA
          </Typography>
          <Box sx={{
            display:
            {
              width: '100%',
            }
          }}>
            <Grid
              container
              spacing={0}
              className='gridButton'
            >
              {pages.map((page) => (
                <Link to={page === 'Contato' ? 'informations' : '/'} className='link'>
                  <Button
                    key={page}
                    sx={{ my: 2, color: 'white', display: 'block' }}
                  >
                    {page}
                  </Button>
                </Link>
              ))}
            </Grid>
          </Box>
        </Container>
      </Container>
    </AppBar>
  )
}
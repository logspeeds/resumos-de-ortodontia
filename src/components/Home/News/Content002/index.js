import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import { CardActionArea } from '@mui/material';

import './style.css'

import imageCard from './mbt.jpg';

export default function Content002() {
  return (
    <Paper elevation24 sx={{ minHeight: '250px', maxHeight: '250px', maxWidth: '250px', margin: '5px' }}>
      <Card sx={{ minHeight: '250px', maxHeight: '250px', maxWidth: '250px' }}>
        <CardActionArea>
          <CardMedia
            component="img"
            height="140"
            image={imageCard}
            alt="Ortodontia"
          />
          <CardContent className='mainBox'>
            <Typography gutterBottom variant="h6" component="div">
              Sistema MBT
            </Typography>
            <Typography className='textBox2' variant="body3" color="text.secondary">
              Também conhecido como aparelho MBT, ele é, na verdade, uma modificação do aparelho convencional que segue o padrão de ...
            </Typography>
            <Divider />
            <Typography className='textBox3' variant="body3" color="text.secondary">
              15 - 03 - 2022
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Paper>
  );
}
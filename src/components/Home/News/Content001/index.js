import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import { CardActionArea } from '@mui/material';

import './style.css'

import imageCard from './ortodontia.jpg';

export default function Content001() {
  return (
    <Paper elevation24 sx={{ minHeight: '250px', maxHeight: '250px', maxWidth: '250px', margin: '5px' }}>
      <Card sx={{ minHeight: '250px', maxHeight: '250px', maxWidth: '250px' }}>
        <CardActionArea>
          <CardMedia
            component="img"
            height="140"
            image={imageCard}
            alt="Ortodontia"
          />
          <CardContent className='mainBox'>
            <Typography gutterBottom variant="h6" component="div">
              Bem-vindo
            </Typography>
            <Typography className='textBox2' variant="body3" color="text.secondary">
              Bem vindo  ao Resumos de ortodontia, esta página busca fazer um banco de dados do curso de ortodontia da PUCPR.
            </Typography>
            <Divider />
            <Typography className='textBox3' variant="body3" color="text.secondary">
              14 - 03 - 2022
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Paper>
  );
}
import * as React from 'react';
import { useState } from "react";
import Content001 from './News/Content001';
import Content002 from './News/Content002';

import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';


import './style.css';

export default function Home() {
  const totalInformations = 12;

  const [scrollX, setscrollX] = useState(-400);

  const handleLeftArrow = () => {
    let x = scrollX + Math.round(window.innerWidth / 2)
    if (x > 0) {
      x = 0
    }
    setscrollX(x)
  }

  const handleRightArrow = () => {
    let x = scrollX - Math.round(window.innerWidth / 2)
    let listW = totalInformations * 150
    if ((window.innerWidth - listW) > x) {
      x = (window.innerWidth - listW) - 60
    }
    setscrollX(x)
  }

  return (
    <main className='mainContent'>
      <div className='mainTitle'>
        <div className='mainText'>ATUALIZAÇÕES</div>

      </div>
      <div className='division'></div>
      <div className='boxContent'>
        <div className="movieRow--left" onClick={handleLeftArrow}>
          <NavigateBeforeIcon style={{ fontSize: 50 }} />
        </div>
        <div className="movieRow--right" onClick={handleRightArrow}>
          <NavigateNextIcon style={{ fontSize: 50 }} />
        </div>
        <div className="movieRow--listArea">
          <div className="movieRow--list"
            style={{
              marginLeft: scrollX,
              width: totalInformations * 150
            }}

          >
            <div className="movieRow--item">
              <Content002 />
            </div>
            <div className="movieRow--item">
              <Content001 />
            </div>
            <div className="movieRow--item">
              <Content002 />
            </div>
            <div className="movieRow--item">
              <Content001 />
            </div>
            <div className="movieRow--item">
              <Content002 />
            </div>
            <div className="movieRow--item">
              <Content001 />
            </div>
            <div className="movieRow--item">
              <Content002 />
            </div>
            <div className="movieRow--item">
              <Content001 />
            </div>
            <div className="movieRow--item">
              <Content002 />
            </div>
            <div className="movieRow--item">
              <Content001 />
            </div>
            <div className="movieRow--item">
              <Content002 />
            </div>
            <div className="movieRow--item">
              <Content001 />
            </div>
          </div>
        </div>
      </div>
    </main >
  );
}
import React from 'react';

import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';

import './style.css'

export default function InformationBar() {

    const textInformation = 'Pontifícia Univeridade Católica do Paraná';

    return (
        <AppBar position="static">
            <div className='root'>
                {textInformation}
            </div>
        </AppBar>
    )
}